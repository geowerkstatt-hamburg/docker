import { generateSimpleGetters } from "../../../src/app-store/utils/generators";
import stateExampleAddon from "./stateExampleAddon";

const getters = {
  ...generateSimpleGetters(stateExampleAddon)
};

export default getters;

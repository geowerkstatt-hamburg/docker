import getters from "./gettersExampleAddon";
import mutations from "./mutationsExampleAddon";
import state from "./stateExampleAddon";

export default {
  namespaced: true,
  dynamic: true,
  state: { ...state },
  mutations,
  getters
};

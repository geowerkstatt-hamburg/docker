import Vuex from "vuex";
import { config, shallowMount, createLocalVue } from "@vue/test-utils";
import ExampleAddonComponent from "../../../components/ExampleAddon.vue";
import ExampleAddon from "../../../store/index";
import { expect } from "chai";


const localVue = createLocalVue();

localVue.use(Vuex);
config.mocks.$t = key => key;

// Test the ExampleAddon component
describe("ExampleAddon/components/ExampleAddon.vue", () => {
  let wrapper;

  it("check mapMode normal -> do render", () => {
    wrapper = createWrapper();
    expect(wrapper.find("#ExampleAddon").exists()).to.be.true;
  });

  const mockConfigJson = {
    Portalconfig: {
      menu: {
        tools: {
          children: {
            ExampleAddon:
            {
              "title": "translate#common:menu.tools.exampleAddon",
              "icon": "glyphicon-resize-small",
              "deactivateGFI": false,
              "renderToWindow": true,
              "active": false
            }
          }
        }
      }
    }
  };
  let store;

  beforeEach(() => {
    store = new Vuex.Store({
      namespaced: true,
      modules: {
        Tools: {
          namespaced: true,
          modules: {
            ExampleAddon
          }
        }
      },
      state: {
        configJson: mockConfigJson
      }
    });
    store.commit("Tools/ExampleAddon/setActive", true);
  });

  // TEARDOWN - run after to each unit test
  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  /**
   * Helper Function to create a wrapper.
   * @return {Object} the shallowMounted wrapper
   */
  function createWrapper() {
    return shallowMount(ExampleAddonComponent, { store, localVue });
  }

});
import ExampleAddonComponent from "./components/ExampleAddon.vue";
import ExampleAddonStore from "./store/index";
import deLocale from "./locales/de/additional.json";
import enLocale from "./locales/en/additional.json";

export default {
    component: ExampleAddonComponent,
    store: ExampleAddonStore,
    locales: {
        de: deLocale,
        en: enLocale
    }
};
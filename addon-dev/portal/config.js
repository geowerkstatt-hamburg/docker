const Config = {
  addons: ["ExampleAddon"],
  namedProjections: [
    ["EPSG:25832", "+title=ETRS89/UTM 32N +proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"]
  ],
  footer: {
    showVersion: true
  },
  layerConf: "./services.json",
  // To keep it simple, we don't need these files
  // but parameters have to point to a real file with some content
  restConf: "./dummy.json",
  styleConf: "./dummy.json"
};

// conditional export to make config readable by e2e tests
if (typeof module !== "undefined") {
  module.exports = Config;
}

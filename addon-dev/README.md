# Dockfile for masterportal addon development

The example addon here is called ExampleAddon and is implemented as *Tool*.

## Build docker image

### For dev branch of masterportal

```
  docker build -t mp-addon-dev .
```

### For a specific version of masterportal

```
  docker build --build-arg MP_VERSION=v2.19.0 -t mp-addon-dev .
```

## Start masterport container and mount neccessary files and folders

### Mounts explanation
<dl>
  <dt>$PWD/addon:/home/masterportal/addons/ExampleAddon</dt>
  <dd>Addon to include. Content of addon folder of this repository is mounted into ExampleAddons folder as subfolder of addons in cloned masterportal.</dd>
  <dt>$PWD/confOverwrite/addonsConf.json:/home/masterportal/addons/addonsConf.json</dt>
  <dd>Definition file of available addons. Needed to transpile addons within masterportal.</dd>
  <dt>$PWD/portal:/home/masterportal/portal</dt>
  <dd>Portal config with activated (config.js) and configured (config.json) addons</dd>
</dl>

### Start development server

```
docker run -it \
           -v $PWD/addon:/home/masterportal/addons/ExampleAddon \
           -v $PWD/confOverwrite/addonsConf.json:/home/masterportal/addons/addonsConf.json \
           -v $PWD/portal:/home/masterportal/portal \
           -p 9001:9001 mp-addon-dev
```

### Run tests in watch mode

```
docker run -it \
           -v $PWD/addon:/home/masterportal/addons/ExampleAddon \
           -v $PWD/confOverwrite/addonsConf.json:/home/masterportal/addons/addonsConf.json \
           -v $PWD/portal:/home/masterportal/portal \
           -p 9001:9001 mp-addon-dev \
           npm run test:all:watch
```

### Start with shell

```
docker run -it \
           -v $PWD/addon:/home/masterportal/addons/ExampleAddon \
           -v $PWD/confOverwrite/addonsConf.json:/home/masterportal/addons/addonsConf.json \
           -v $PWD/portal:/home/masterportal/portal \
           -p 9001:9001 mp-addon-dev \
           npm run test:all:watch
```

## Notes
- [Masterportal addon tutorial](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/addOnsVue.md)
- **The name of the addon in *config.json* must be in lowerCamelCase regardless of all other definitions in this repository. This behavior is enforced by the masterportal addon integration mechanism.**
- The folder *portal* contains the most basic configuration for a masterportal instance.
- The addon name must be added to [portal/config.js](./portal/config.js#L2)
- The addon must be configured in [portal/config.json](./portal/config.json#L17)

